image_width=20/1000;            %  Size of image sector
fs=100e6;                %  Sampling frequency [Hz]
c=1540;                  %  Speed of sound [m/s]
min_sample = 0;
no_lines = 50;
d_x=image_width/no_lines;       %  Increment for image

for i=1:no_lines
    
    %  Load the result
    disp(['load rf_data/psf/rf_ln',num2str(i),'.mat'])
    load(['rf_data/psf/rf_ln',num2str(i),'.mat'],'rf_data','tstart');
    
    % Find the PSF
    psf_i=[zeros(round(tstart*fs-min_sample),1); rf_data];
    psf(1:max(size(psf_i)),i)=psf_i;
    
    %  Find the envelope
    rf_env=abs(hilbert([zeros(round(tstart*fs-min_sample),1); rf_data]));
    env(1:max(size(rf_env)),i)=rf_env;
end

clf
figure(1)
subplot(121)
x_m = linspace(-image_width,image_width,no_lines);
z_m = ((1:size(psf,1))./fs)*1540/2;
imagesc(x_m,z_m,psf)
xlabel('Lateral distance [m]')
ylabel('Axial distance [m]')
colormap(gray)
axis([-5 5 60 61.5]./1000)

subplot(122)
k_space = fft2(psf);
kz = linspace(-0.5,0.5,length(z_m)).*fs./(c./2);
kx = linspace(-0.5,0.5,length(x_m)).*(1./d_x);
contour(kx,kz,fftshift(abs(k_space)))
xlabel('kx [1/meter]')
ylabel('kz [1/meter]')
axis([-6500 6500 -6500 6500])