# Field ii example PSF and simulated image

- Demonstrates both serial execution and parallel SLURM array job
- Creates point spread function and phantom image

## To do this the 'slow' way (without using cluster)
- run sim_img.m or sim_psf.m in matlab 

## To use the fast method (using array job on cluster)
- Make phantom using  mk_pht.m file, which should save a file called pht_data.mat
- Open a command window and navigate to a copy of this repository, which should be saved on a directory which has access to the cluster
- Modify launch.sh to only do sim_img or sim_psf if you only want it do one of them (its currently set to do both). You can also change the number of lines in the image (currently at 50) by changing --array=1-50
- Modify sim_psf_fast.sh and/or sim_img_fast.sh if you would like to receive an email notification when its's done or change other aspects of how the simulation will be run on the cluster
- Modify sim_psf_fast.m and/or sim_img_fast.m to include the path to your version of field II. Also make sure that no_lines matches the number of lines specified in launch.sh (i.e. the range of the array). 
- Type 'ssh durmstrang.egr.duke.edu' in command window, and enter password
- Enter ./launch.sh in command window 
- If something goes wrong, look under the output folder in the directory for error logs (.err files will give you errors associated with running it on the cluster, .out files will show you Matlab errors and outputs). 
- Run dsp_img.m and/or disp_psf.m in Matlab to image and point spread function, respectively. 
  

