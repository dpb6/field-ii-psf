%  This example shows a linear array psf
%
%  Here the field simulation is performed and the data is stored
%  in rf-files; one for each rf-line done. The data must then
%  subsequently be processed to yield the image. The data for the
%  scatteres are read from the file pht_data.mat, so that the procedure
%  can be started again or run for a number of workstations.
%
%  Example by Joergen Arendt Jensen and Peter Munk, 
%  Version 1.2, August 14, 1998, JAJ.

%  Ver. 1.1: 1/4-98: Procedure xdc_focus_center inserted to use the new
%                    focusing scheme for the Field II program
%  Ver. 2.0: 13/8 2007: Parallel version that checks whether the simulation
%                       of a line has been made before, which makes it possible
%                       to run the code in parallel on multiple workstations.
field_init(-1)

%  Generate the transducer apertures for send and receive
f0=3.5e6;                %  Transducer center frequency [Hz]
fs=100e6;                %  Sampling frequency [Hz]
c=1540;                  %  Speed of sound [m/s]
lambda=c/f0;             %  Wavelength [m]
width=lambda;            %  Width of element
element_height=5/1000;   %  Height of element [m]
kerf=0.05/1000;          %  Kerf [m]
focus=[0 0 70]/1000;     %  Fixed focal point [m]
N_elements=192;          %  Number of physical elements
N_active=64;             %  Number of active elements 

%  Set the sampling frequency
set_sampling(fs);

%  Generate aperture for emission
xmit_aperture = xdc_linear_array (N_elements, width, element_height, kerf, 1, 10,focus);

%  Set the impulse response and excitation of the xmit aperture
impulse_response=sin(2*pi*f0*(0:1/fs:2/f0));
impulse_response=impulse_response.*hanning(max(size(impulse_response)))';
xdc_impulse (xmit_aperture, impulse_response);
excitation=sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation (xmit_aperture, excitation);

%  Generate aperture for reception
receive_aperture = xdc_linear_array (N_elements, width, element_height, kerf, 1, 10,focus);

%  Set the impulse response for the receive aperture
xdc_impulse (receive_aperture, impulse_response);

%   Load the computer phantom
phantom_positions=[0,0,60]./1000;
phantom_amplitudes=1;

%  Set the different focal zones for reception
focal_zones=[30:20:200]'/1000;
Nf=max(size(focal_zones));
focus_times=(focal_zones-10/1000)/1540;
z_focus=60/1000;          %  Transmit focus

%  Set the apodization
apo=hanning(N_active)';

%   Do linear array imaging
no_lines=50;                    %  Number of lines in image
image_width=20/1000;            %  Size of image sector
d_x=image_width/no_lines;       %  Increment for image

% Do imaging line by line
for i=[1:no_lines]

  %  Test if the file for the line exist.
  %  Skip the simulation, if the line exits and
  %  go the next line. Else make the simulation
  file_name=['rf_data/rf_ln',num2str(i),'.mat'];

  if ~exist('rf_data','dir')
      mkdir('rf_data')
  end
    
  if ~exist(file_name)
    
    %  Save a file to reserve the calculation
    cmd=['save rf_data/rf_ln',num2str(i),'.mat i'];
    eval(cmd);
    disp(['Now making line ',num2str(i)])
  
    %  The the imaging direction
    x= -image_width/2 +(i-1)*d_x;

    %   Set the focus for this direction with the proper reference point
    xdc_center_focus (xmit_aperture, [x 0 0]);
    xdc_focus (xmit_aperture, 0, [x 0 z_focus]);
    xdc_center_focus (receive_aperture, [x 0 0]);
    xdc_focus (receive_aperture, focus_times, [x*ones(Nf,1), zeros(Nf,1), focal_zones]);

    %  Calculate the apodization 
    N_pre  = round(x/(width+kerf) + N_elements/2 - N_active/2);
    N_post = N_elements - N_pre - N_active;
    apo_vector=[zeros(1,N_pre) apo zeros(1,N_post)];
    xdc_apodization (xmit_aperture, 0, apo_vector);
    xdc_apodization (receive_aperture, 0, apo_vector);
  
    %   Calculate the received response
    [rf_data, tstart] = calc_scat(xmit_aperture, receive_aperture, phantom_positions, phantom_amplitudes);
    [hhp, tstart2]    = calc_hhp(xmit_aperture, receive_aperture, phantom_positions);

    %  Store the result
    cmd=['save rf_data/rf_ln',num2str(i),'.mat rf_data tstart'];
    disp(cmd)
    eval(cmd);
  else
    disp(['Line ',num2str(i),' is being made by another machine.'])
    end
end

%   Free space for apertures
xdc_free (xmit_aperture)
xdc_free (receive_aperture)
field_end

%  Read the data and adjust it in time
min_sample=0;
for i=1:no_lines
    
    %  Load the result
    disp(['load rf_data/rf_ln',num2str(i),'.mat'])
    load(['rf_data/rf_ln',num2str(i),'.mat'],'rf_data','tstart');
    
    % Find the PSF
    psf_i=[zeros(round(tstart*fs-min_sample),1); rf_data];
    psf(1:max(size(psf_i)),i)=psf_i;
    
    %  Find the envelope
    rf_env=abs(hilbert([zeros(round(tstart*fs-min_sample),1); rf_data]));
    env(1:max(size(rf_env)),i)=rf_env;
end

clf
figure(1)
subplot(121)
x_m = linspace(-image_width,image_width,no_lines);
z_m = ((1:size(psf,1))./fs)*1540/2;
imagesc(x_m,z_m,psf)
xlabel('Lateral distance [m]')
ylabel('Axial distance [m]')
colormap(gray)
axis([-5 5 60 61.5]./1000)

subplot(122)
k_space = fft2(psf);
kz = linspace(-0.5,0.5,length(z_m)).*fs./(c./2);
kx = linspace(-0.5,0.5,length(x_m)).*(1./d_x);
contour(kx,kz,fftshift(abs(k_space)))
xlabel('kx [1/meter]')
ylabel('kz [1/meter]')
axis([-6500 6500 -6500 6500])
