%  Compress the data to show 60 dB of
%  dynamic range for the cyst phantom image
%
%  version 1.3 by Joergen Arendt Jensen, April 1, 1998.
%  version 1.4 by Joergen Arendt Jensen, August 13, 2007.
%          Clibrated 60 dB display made
if ~exist('results.mat')

    f0=3.5e6;                 %  Transducer center frequency [Hz]
    fs=100e6;                 %  Sampling frequency [Hz]
    c=1540;                   %  Speed of sound [m/s]
    no_lines=50;              %  Number of lines in image
    image_width=40/1000;      %  Size of image sector
    d_x=image_width/no_lines; %  Increment for image
    
    %  Read the data and adjust it in time 
    min_sample=0;
    for i=1:no_lines

      %  Load the result

      cmd=['load rf_data/rf_ln',num2str(i),'.mat'];
      disp(cmd)
      eval(cmd)

      % Find the PSF
      psf_i=[zeros(round(tstart*fs-min_sample),1); rf_data];
      psf(1:max(size(psf_i)),i)=psf_i;
      
      %  Find the envelope
      rf_env=abs(hilbert([zeros(round(tstart*fs-min_sample),1); rf_data]));
      env(1:max(size(rf_env)),i)=rf_env;
    end

    clf
    x_mm = linspace(-image_width,image_width,no_lines).*1000;
    z_mm = ((1:size(psf,1))./fs)*1540/2*1000;
    imagesc(x_mm,z_mm,psf)
    xlabel('Lateral distance [mm]')
    ylabel('Axial distance [mm]')
    colormap(gray)
    %axis('image')
    axis([-10 10 59 63])

    k_space = fft2(psf);
    contour(fftshift(abs(k_space)))
    kz = linspace(-0.5,0.5,length(z_mm)).*fs./(c./2);
    kx = linspace(-0.5,0.5,length(x_mm)).*(1./d_x);
    
    contour(kx,kz,fftshift(abs(k_space)))
    
    %  Do logarithmic compression
    D=10;   %  Sampling frequency decimation factor
    disp('Finding the envelope')
    log_env=env(1:D:max(size(env)),:)/max(max(env));
    log_env=20*log10(log_env);
    log_env=127/60*(log_env+60);

    %  Make an interpolated image
    disp('Doing interpolation')
    ID=20;
    [n,m]=size(log_env);
    new_env=zeros(n,m*ID);
    for i=1:n
      new_env(i,:)=abs(interp(log_env(i,:),ID));
    end
    [n,m]=size(new_env);

    fn=fs/D;
    clf
    save results ID no_lines d_x n fn min_sample fs new_env

else
    load results
    image(((1:(ID*no_lines-1))*d_x/ID-no_lines*d_x/2)*1000,((1:n)/fn+min_sample/fs)*1540/2*1000,new_env)
    xlabel('Lateral distance [mm]')
    ylabel('Axial distance [mm]')
    colormap(gray(127))
    axis('image')
    axis([-20 20 35 90])
end
